import 'package:flutter/material.dart';
import '../widgets/responsive_safe_area.dart';
import 'dart:math' as math;

class IAmPoor extends StatefulWidget {
  @override
  _IAmPoorState createState() => _IAmPoorState();
}

class _IAmPoorState extends State<IAmPoor> with SingleTickerProviderStateMixin {
  AnimationController animated;

  @override
  void initState() {
    super.initState();
    animated = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 333),
    );
    animated.forward();
  }

  @override
  void dispose() {
    animated.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text('Poor'),
        centerTitle: true,
        backgroundColor: Colors.blueGrey,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: animate,
        child: const Icon(Icons.play_arrow),
      ),
      body: Container(
        alignment: Alignment.center,
        child: GestureDetector(
          onTap: animate,
          child: AnimatedBuilder(
            animation: animated,
            builder: (BuildContext context, Widget child) => Transform(
              alignment: Alignment.center,
              transform: Matrix4.identity()
                ..rotateZ(
                  math.pi * 2 * animated.value,
                ),
              child: child,
            ),
            child: Image.asset(
              'assets/cat.png',
              width: size.width / 2,
            ),
          ),
        ),
      ),
    );
  }

  void animate() {
    if (animated.isCompleted) {
      animated.reset();
      animated.forward();
    } else if (animated.isDismissed) {
      animated.forward();
    }
  }
}
