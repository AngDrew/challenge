import 'package:flutter/material.dart';
import '../widgets/responsive_safe_area.dart';

class Layouting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ResponsiveSafeArea(
        builder: (BuildContext context, Size size) => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              height: double.infinity,
              width: 100,
              color: Colors.red,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 100,
                  height: 100,
                  color: Colors.yellow[900],
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Colors.yellow[400],
                ),
              ],
            ),
            Container(
              height: double.infinity,
              width: 100,
              color: Colors.blue,
            )
          ],
        ),
      ),
    );
  }
}
