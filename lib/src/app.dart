import 'package:challenge/src/challenges/iampoor.dart';
import 'package:challenge/src/challenges/layouting.dart';
import 'package:flutter/material.dart';

import 'widgets/responsive_safe_area.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: size.width * 0.5,
            child: RaisedButton(
              onPressed: () {
                Navigator.push<void>(
                  context,
                  MaterialPageRoute<IAmPoor>(
                    builder: (BuildContext context) => IAmPoor(),
                  ),
                );
              },
              child: const Text('I Am Poor Challenge'),
            ),
          ),
          //
          SizedBox(
            height: 50,
            width: size.width,
          ),
          //
          Container(
            width: size.width * 0.5,
            child: RaisedButton(
              onPressed: () {
                Navigator.push<void>(
                  context,
                  MaterialPageRoute<IAmPoor>(
                    builder: (BuildContext context) => Layouting(),
                  ),
                );
              },
              child: const Text('Layouting Challenge'),
            ),
          ),
        ],
      ),
    );
  }
}
